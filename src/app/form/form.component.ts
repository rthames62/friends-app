import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addFriend } from '../store/friends/friends.actions';
import { Friend } from '../types/Friend';
import { AppState } from '../store/app.state';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  // For performance reasons, using OnPush
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  public form: FormGroup;

  constructor(
    /*
      There was an issue when providing the store in the spec
      so the workaround was to directly inject the store into
      the component. This would be labled a TeuxDeux to fix
      or to pair to get a separate pair of eyes on it. Didnt
      want to spend too much time on this for the assessment
    */
    @Inject(Store) private store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      age: new FormControl('', [
        Validators.required,
        Validators.min(1),
      ]),
      weight: new FormControl('', [
        Validators.required,
        Validators.min(1),
      ]),
      friends: new FormControl('', [
        Validators.required,
        Validators.min(1),
      ]),
    });
  }

  public addFriend(): void {
    const { name, age, weight, friends } = this.form.controls;
    const friend: Friend = {
      name: name.value,
      age: age.value,
      weight: weight.value,
      friends: friends.value,
    };
    // Dispatching an action to add the friend to the store
    this.store.dispatch(addFriend({ friend }));
    // Resetting the form after submitted
    this.form.reset();
  }
}
