import { provideMockStore } from '@ngrx/store/testing';
import { FormComponent } from './form.component';
import { Shallow } from 'shallow-render';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, Form } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSidenavModule } from '@angular/material/sidenav';
import { AppState } from '../store/app.state';
import { Store } from '@ngrx/store';

const mockStore = {
  select: jest.fn(),
  dispatch: jest.fn(),
};

describe('FormComponent', () => {
  let shallow: Shallow<FormComponent>;

  @NgModule({
    declarations: [
      FormComponent,
    ],
    imports: [
      MatInputModule,
      MatButtonModule,
      MatFormFieldModule,
      MatSidenavModule,
      ReactiveFormsModule,
    ],
    providers: [
      provideMockStore<AppState>(),
      { provide: Store, useValue: mockStore },
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  })
  class TestModule { }

  beforeEach(() => {
    shallow = new Shallow(FormComponent, TestModule);
    shallow.dontMock(
      Store,
    );
  });

  describe('when changing the form', () => {
    it('should check validity', async () => {
      const { instance, fixture } = await shallow.render();
      const { controls } = instance.form;
      expect(controls.name.valid).toBeFalsy();
      expect(controls.age.valid).toBeFalsy();
      expect(controls.weight.valid).toBeFalsy();
      expect(controls.friends.valid).toBeFalsy();
      expect(instance.form.valid).toBeFalsy();

      controls.name.setValue('Jon Snow');
      fixture.detectChanges();
      expect(controls.name.valid).toBe(true);

      controls.age.setValue(31);
      fixture.detectChanges();
      expect(controls.age.valid).toBe(true);

      controls.weight.setValue(180);
      fixture.detectChanges();
      expect(controls.weight.valid).toBe(true);

      controls.friends.setValue(2);
      fixture.detectChanges();
      expect(controls.friends.valid).toBe(true);

      expect(instance.form.valid).toBe(true);
    });
  });

  describe('when adding a friend', () => {
    it('should dispatch action and reset form', async () => {
      const { find, fixture, instance } = await shallow.render();
      const submitBtn = find('button');
      const addFriendsSpy = jest.spyOn(instance, 'addFriend');
      const dispatchSpy = jest.spyOn(mockStore, 'dispatch');
      const resetSpy = jest.spyOn(instance.form, 'reset');
      instance.form.controls.name.setValue('Jon Snow');
      instance.form.controls.age.setValue(30);
      instance.form.controls.weight.setValue(200);
      instance.form.controls.friends.setValue(2);

      instance.form.updateValueAndValidity();
      submitBtn.triggerEventHandler('click', {});
      fixture.detectChanges();

      expect(addFriendsSpy).toHaveBeenCalled();
      expect(dispatchSpy).toHaveBeenCalledWith({
        type: '[Friends] Add Friend',
        friend: {
          name: 'Jon Snow',
          age: 30,
          weight: 200,
          friends: 2,
        }
      });
      expect(resetSpy).toHaveBeenCalled();
    });
  });
});
