import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { StoreModule } from '@ngrx/store';
import { MatFormFieldModule } from '@angular/material/form-field';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { ChartComponent } from './chart/chart.component';
import * as fromFriends from './store/friends/friends.reducer';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(
      { friendsState: fromFriends.reducer },
    ),
    NoopAnimationsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
