export interface Friend {
  name: string;
  friends: number;
  age: number;
  weight: number;
}
