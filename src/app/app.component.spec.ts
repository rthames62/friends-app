import { AppComponent } from './app.component';
import { Shallow } from 'shallow-render';
import { NgModule } from '@angular/core';
import { FormComponent } from './form/form.component';
import { ChartComponent } from './chart/chart.component';
import { MatSidenavModule } from '@angular/material/sidenav';

describe('AppComponent', () => {
  let shallow: Shallow<AppComponent>;

  @NgModule({
    declarations: [
      AppComponent,
      FormComponent,
      ChartComponent,
    ],
    imports: [
      MatSidenavModule,
    ]
  })
  class TestModule {}

  beforeEach(() => {
    shallow = new Shallow(AppComponent, TestModule);
  });

  it('should render the proper components', async () => {
    const { find } = await shallow.render();
    expect(find('mat-drawer-container')).toHaveFoundOne();
    expect(find('app-form')).toHaveFoundOne();
    expect(find('app-chart')).toHaveFoundOne();
  });
});
