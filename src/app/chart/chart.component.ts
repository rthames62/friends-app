import { Component, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit, Inject, OnDestroy, InjectionToken } from '@angular/core';
import * as d3 from 'd3';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app.state';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { Friend } from '../types/Friend';
import { selectFriends } from '../store/friends/friends.selectors';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChartComponent implements AfterViewInit, OnDestroy {
  @ViewChild('chart') public chart: ElementRef;

  public friends: Friend[];

  public destroyed$ = new Subject();

  // D3 properties
  /*
    Margin conventions as recommended here:
    https://observablehq.com/@d3/margin-convention
  */
  public margin = { top: 20, right: 30, bottom: 30, left: 40 };
  public width = 800 - this.margin.left - this.margin.right;
  public height = 500 - this.margin.top - this.margin.bottom;
  public xScale;
  public yScale;

  constructor(
    @Inject(Store) private store: Store<AppState>,
  ) { }

  public ngAfterViewInit(): void {
    this.store.select(selectFriends).pipe(
      /*
        This is not necessary in this use case since
        this component never gets destroyed, but it is
        good practice to close off observables
      */
      takeUntil(this.destroyed$),
      tap((friends) => {
        this.friends = friends;
        this.clearChart();
        this.renderChart();
      })
    ).subscribe();
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  public clearChart(): void {
    d3.select('svg').remove();
  }

  /*
    Note: There is a lot of refactoring that I could
    do in this method. I have a lot more brushing up
    on D3 in order to approach this in an optimal way.

    Specifically, I was going down a rabbit hole on
    the best way to organize each section of bars
    since there were 3 data points per object. The
    approach I took limited myself from being able
    to properly center the group on the tick of the
    X axis.

    My solution would be to dig deeper in the D3 docs
    and examples (when time is on my side) or ask
    another engineer to pair for fresh eyes.
  */
  public renderChart(): void {
    const l = this.friends.length;
    // Basing extent on the the hightest number in the array
    const yMax = d3.extent(this.friends, (friend) => {
      const numbers =  Object.keys(friend)
        .filter(key => key !== 'name')
        .map(key => friend[key]);

      return Math.max(...numbers);
    });

    this.xScale = d3.scaleBand()
      .domain(this.friends.map(friend => friend.name))
      .rangeRound([0, this.width])
      .paddingInner(.1);

    this.yScale = d3.scaleLinear()
      .domain([0, yMax[1]])
      .range([0, this.height]);

    const yScaleForAxis = d3.scaleLinear()
    .domain([yMax[1], 0])
    .range([0, this.height]);

    const svg = d3.select(this.chart.nativeElement)
      .append('svg')
      .attr('width', this.width + this.margin.left + this.margin.right)
      .attr('height', this.height + this.margin.top + this.margin.bottom)
      .append('g')
      .attr('class', 'container')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    const bandwidth = this.xScale.bandwidth();
    const barWidth = bandwidth / 3.333;

    // Render the bar containers for each domain
    const barContainers = svg.selectAll('.bar')
      .data(this.friends)
      .enter()
      .append('g')
      .attr('class', 'bar')
      .attr('transform', friend => `translate(${this.xScale(friend.name) + (barWidth * 2) + 10}, 0)`);

    // Render the bar for age
    barContainers.append('rect')
      .attr('class', 'age')
      .attr('width', barWidth)
      .attr('transform', `translate(${-barWidth}, 0)rotate(180)`)
      .attr('y', -this.height)
      .style('fill', 'blue')
      .attr('height', 0)
      .transition()
      .duration(500)
      .attr('height', friend => this.yScale(friend.age));

    // Render the bar for weight
    barContainers.append('rect')
      .attr('class', 'weight')
      .attr('width', barWidth)
      .attr('transform', 'rotate(180)')
      .attr('y', -this.height)
      .attr('height', 0)
      .style('fill', 'green')
      .transition()
      .duration(500)
      .delay(500)
      .attr('height', friend => this.yScale(friend.weight));

    // Render the bar for friends
    barContainers.append('rect')
      .attr('class', 'friends')
      .attr('width', barWidth)
      .attr('transform', `translate(${barWidth}, 0)rotate(180)`)
      .attr('y', -this.height)
      .attr('height', 0)
      .style('fill', 'pink')
      .transition()
      .duration(500)
      .delay(1000)
      .attr('height', friend => this.yScale(friend.friends));

    // Render the axis'
    const xAxis = d3.axisBottom(this.xScale);

    svg.append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(0, ${this.height + 10})`)
      .call(xAxis);

    const yAxis = d3.axisLeft(yScaleForAxis);

    svg.append('g')
      .attr('class', 'y-axis')
      .attr('transform', `translate(-10, 0)`)
      .call(yAxis);
  }
}
