import { ChartComponent } from './chart.component';
import { Shallow } from 'shallow-render';
import { NgModule } from '@angular/core';
import { provideMockStore, MockSelector, MockStore } from '@ngrx/store/testing';
import { AppState } from '../store/app.state';
import { Store } from '@ngrx/store';
import { selectFriends } from '../store/friends/friends.selectors';
import { Subject, BehaviorSubject } from 'rxjs';
import { tick, fakeAsync } from '@angular/core/testing';

const mockFriends = [
    {
      name: 'Harry Potter',
      age: 16,
      weight: 180,
      friends: 2,
    },
    {
      name: 'Hermione Granger',
      age: 17,
      weight: 112,
      friends: 3,
    },
];

const mockStore = {
  select: jest.fn().mockReturnValue(new BehaviorSubject(mockFriends)),
  dispatch: jest.fn(),
};

const mockSelectFriends: MockSelector = {
  selector: selectFriends,
  value: mockFriends,
};

const mockStoreProviders = provideMockStore<AppState>({
  selectors: [ mockSelectFriends ],
})

describe('ChartComponent', () => {
  let shallow: Shallow<ChartComponent>;

  @NgModule({
    declarations: [
      ChartComponent,
    ],
    providers: [
      mockStoreProviders,
      { provide: Store, useValue: mockStore },
    ],
  })
  class TestModule { }

  beforeEach(() => {
    shallow = new Shallow(ChartComponent, TestModule);
    shallow.dontMock(Store);

  });

  describe('After view init', () => {
    it('should subscribe to the selector and call methods', async () => {
      const { instance } = await shallow.render();
      const renderChartSpy = jest.spyOn(instance, 'renderChart');
      const clearChartSpy = jest.spyOn(instance, 'clearChart');
      instance.ngAfterViewInit();
      expect(renderChartSpy).toHaveBeenCalled();
      expect(clearChartSpy).toHaveBeenCalled();
      expect(instance.friends).toBe(mockFriends);
    });
  });

  describe('when the chart renders', () => {
    /*
      Once again, there may be a better way to test the DOM
      manipulation inside the component, but this seemed to
      be the easiest way without putting too much effort
      into it. It is all scoped into 1 it block because of
      the amount of drilling down into the nativeElement
      of the chart.
    */
    it('should render the proper elements', async () => {
      const { instance } = await shallow.render();
      const chart = instance.chart.nativeElement;

      // Testing the svg being rendered
      const svg = [...chart.children].find(child => child.tagName === 'svg');
      const svgWidth = [...svg.attributes].find(attr => attr.name === 'width').value;
      const svgHeight = [...svg.attributes].find(attr => attr.name === 'height').value;

      expect(svgWidth).toBe(`${instance.width + instance.margin.left + instance.margin.right}`);
      expect(svgHeight).toBe(`${instance.height + instance.margin.top + instance.margin.bottom}`);

      // Testing the g.container being rendered
      const gContainer = [...svg.children].find(child => child.className.baseVal === 'container');
      const gContainerTranslate = [...gContainer.attributes].find(attr => attr.name === 'transform').value;

      expect(gContainerTranslate).toBe(`translate(${instance.margin.left}, ${instance.margin.top})`);

      // Testing the g.x-axis element being rendered
      const xAxis = [...gContainer.children].find(child => child.className.baseVal === 'x-axis');
      const ticks = [...xAxis.children].filter(child => child.className.baseVal === 'tick');

      expect(ticks[0].textContent).toBe(mockFriends[0].name);
      expect(ticks[1].textContent).toBe(mockFriends[1].name);

      // Testing the g.bar elements being rendered
      const gbars = [...gContainer.children].filter(child => child.className.baseVal === 'bar');
      const barWidth = instance.xScale.bandwidth() / 3.333;
      const bar0Translate = [...gbars[0].attributes].find(attr => attr.name === 'transform').value;
      const bar1Translate = [...gbars[1].attributes].find(attr => attr.name === 'transform').value;

      expect(gbars.length).toBe(mockFriends.length);
      expect(bar0Translate).toBe(
        `translate(${instance.xScale(gbars[0].__data__.name) + (barWidth * 2) + 10}, 0)`
      );
      expect(bar1Translate).toBe(
        `translate(${instance.xScale(gbars[1].__data__.name) + (barWidth * 2) + 10}, 0)`
      );

      // Testing the age bars being rendered
      const ageBar0 = [...gbars[0].children].find(child => child.className.baseVal === 'age');

      const ageBar0Translate = [...ageBar0.attributes].find(attr => attr.name === 'transform').value;
      const ageBar0Width = [...ageBar0.attributes].find(attr => attr.name === 'width').value;
      const ageBar0Height = [...ageBar0.attributes].find(attr => attr.name === 'height').value;
      const ageBar0Y = [...ageBar0.attributes].find(attr => attr.name === 'y').value;
      const ageBar0Fill = [...ageBar0.attributes].find(attr => attr.name === 'style').value;

      expect(ageBar0Translate).toBe(`translate(${-barWidth}, 0)rotate(180)`);
      expect(ageBar0Width).toBe(`${barWidth}`);
      expect(ageBar0Y).toBe(`${-instance.height}`);
      // Simulating transition duration
      setTimeout(() => expect(`${ageBar0Height}`).toEqual(instance.yScale(ageBar0.__data__.name)), 500);
      expect(ageBar0Fill).toContain('fill: blue');

      const ageBar1 = [...gbars[1].children].find(child => child.className.baseVal === 'age');

      const ageBar1Translate = [...ageBar1.attributes].find(attr => attr.name === 'transform').value;
      const ageBar1Width = [...ageBar1.attributes].find(attr => attr.name === 'width').value;
      const ageBar1Height = [...ageBar1.attributes].find(attr => attr.name === 'height').value;
      const ageBar1Y = [...ageBar1.attributes].find(attr => attr.name === 'y').value;
      const ageBar1Fill = [...ageBar1.attributes].find(attr => attr.name === 'style').value;

      expect(ageBar1Translate).toBe(`translate(${-barWidth}, 0)rotate(180)`);
      expect(ageBar1Width).toBe(`${barWidth}`);
      expect(ageBar1Y).toBe(`${-instance.height}`);
      // Simulating transition duration
      setTimeout(() => expect(`${ageBar1Height}`).toEqual(instance.yScale(ageBar1.__data__.name)), 1000);
      expect(ageBar1Fill).toContain('fill: blue');

      // Testing the weight bars being rendered
      const weightBar0 = [...gbars[0].children].find(child => child.className.baseVal === 'weight');

      const weightBar0Translate = [...weightBar0.attributes].find(attr => attr.name === 'transform').value;
      const weightBar0Width = [...weightBar0.attributes].find(attr => attr.name === 'width').value;
      const weightBar0Height = [...weightBar0.attributes].find(attr => attr.name === 'height').value;
      const weightBar0Y = [...weightBar0.attributes].find(attr => attr.name === 'y').value;
      const weightBar0Fill = [...weightBar0.attributes].find(attr => attr.name === 'style').value;

      expect(weightBar0Translate).toBe(`rotate(180)`);
      expect(weightBar0Width).toBe(`${barWidth}`);
      expect(weightBar0Y).toBe(`${-instance.height}`);
      // Simulating transition duration
      setTimeout(() => expect(`${weightBar0Height}`).toEqual(instance.yScale(weightBar0.__data__.name)), 500);
      expect(weightBar0Fill).toContain('fill: green');

      const weightBar1 = [...gbars[1].children].find(child => child.className.baseVal === 'weight');

      const weightBar1Translate = [...weightBar1.attributes].find(attr => attr.name === 'transform').value;
      const weightBar1Width = [...weightBar1.attributes].find(attr => attr.name === 'width').value;
      const weightBar1Height = [...weightBar1.attributes].find(attr => attr.name === 'height').value;
      const weightBar1Y = [...weightBar1.attributes].find(attr => attr.name === 'y').value;
      const weightBar1Fill = [...weightBar1.attributes].find(attr => attr.name === 'style').value;

      expect(weightBar1Translate).toBe(`rotate(180)`);
      expect(weightBar1Width).toBe(`${barWidth}`);
      expect(weightBar1Y).toBe(`${-instance.height}`);
      // Simulating transition duration
      setTimeout(() => expect(`${weightBar1Height}`).toEqual(instance.yScale(weightBar1.__data__.name)), 1000);
      expect(weightBar1Fill).toContain('fill: green');

      // Testing the weight bars being rendered
      const friendsBar0 = [...gbars[0].children].find(child => child.className.baseVal === 'friends');

      const friendsBar0Translate = [...friendsBar0.attributes].find(attr => attr.name === 'transform').value;
      const friendsBar0Width = [...friendsBar0.attributes].find(attr => attr.name === 'width').value;
      const friendsBar0Height = [...friendsBar0.attributes].find(attr => attr.name === 'height').value;
      const friendsBar0Y = [...friendsBar0.attributes].find(attr => attr.name === 'y').value;
      const friendsBar0Fill = [...friendsBar0.attributes].find(attr => attr.name === 'style').value;

      expect(friendsBar0Translate).toBe(`translate(${barWidth}, 0)rotate(180)`);
      expect(friendsBar0Width).toBe(`${barWidth}`);
      expect(friendsBar0Y).toBe(`${-instance.height}`);
      // Simulating transition duration
      setTimeout(() => expect(`${friendsBar0Height}`).toEqual(instance.yScale(friendsBar0.__data__.name)), 500);
      expect(friendsBar0Fill).toContain('fill: pink');

      const friendsBar1 = [...gbars[1].children].find(child => child.className.baseVal === 'friends');

      const friendsBar1Translate = [...friendsBar1.attributes].find(attr => attr.name === 'transform').value;
      const friendsBar1Width = [...friendsBar1.attributes].find(attr => attr.name === 'width').value;
      const friendsBar1Height = [...friendsBar1.attributes].find(attr => attr.name === 'height').value;
      const friendsBar1Y = [...friendsBar1.attributes].find(attr => attr.name === 'y').value;
      const friendsBar1Fill = [...friendsBar1.attributes].find(attr => attr.name === 'style').value;

      expect(friendsBar1Translate).toBe(`translate(${barWidth}, 0)rotate(180)`);
      expect(friendsBar1Width).toBe(`${barWidth}`);
      expect(friendsBar1Y).toBe(`${-instance.height}`);
      // Simulating transition duration
      setTimeout(() => expect(`${friendsBar1Height}`).toEqual(instance.yScale(friendsBar1.__data__.name)), 1000);
      expect(friendsBar1Fill).toContain('fill: pink');
    });
  });
});
