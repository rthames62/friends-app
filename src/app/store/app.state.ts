import { Friend } from '../types/Friend';

export interface AppState {
  friendsState: FriendsState;
}

export interface FriendsState {
  friends: Friend[];
}

export const initialState: FriendsState = {
  friends: [],
};
