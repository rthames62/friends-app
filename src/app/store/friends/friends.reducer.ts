import { createReducer, on, Action } from '@ngrx/store';
import { addFriend } from './friends.actions';
import { initialState, FriendsState } from '../app.state';

const friendsReducer = createReducer(
  initialState,
  on(addFriend, (state, { friend }) => {
    const friends = [friend, ...state.friends];
    return {
      friends,
    };
  }),
);

export function reducer(
  state: FriendsState | undefined,
  action: Action,
) {
  return friendsReducer(state, action);
}
