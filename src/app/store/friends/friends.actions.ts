import { createAction, props } from '@ngrx/store';
import { Friend } from 'src/app/types/Friend';

export const addFriend = createAction(
  '[Friends] Add Friend',
  props<{ friend: Friend }>(),
);
