import { AppState, FriendsState } from '../app.state';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export const friendsStateSelector = (state: AppState) => state.friendsState;

export const selectFriends = createSelector(
  friendsStateSelector,
  (state: FriendsState) => state.friends,
);
